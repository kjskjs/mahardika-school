## Mahardika School

### Installation
Please run this in console in order
```
cp .env.example .env
composer install
php artisan key:generate
php artisan migrate
php artisan passport:install
```

You can run `php artisan db:seed` in case fake data load needed.

### Documentation
Please take a look at our short [introduction](/public/introduction.pdf) documentation and please download the postman [collection](/public/Mahardika School API Docs.postman_collection.json) and [environment](/public/Mahardika School Environement.postman_environment.json) to use.
