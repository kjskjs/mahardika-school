<?php

namespace App\Entities\Constants;

class CommonDatabaseFields extends CommonEnum
{
    const ID = "id";
    const CREATED_AT = "created_at";
    const UPDATED_AT = "updated_at";
    const CREATED_BY = "created_by";
    const UPDATED_BY = "updated_by";
    const DELETED_AT = "updated_at";
}
