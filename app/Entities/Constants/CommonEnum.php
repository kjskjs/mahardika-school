<?php

namespace App\Entities\Constants;

use ReflectionClass;

abstract class CommonEnum
{
    private static $constCacheArray = NULL;

    public static function get()
    {
        if (self::$constCacheArray == NULL) {
            self::$constCacheArray = [];
        }
        $calledClass = get_called_class();
        if (!array_key_exists($calledClass, self::$constCacheArray)) {
            $reflect = new ReflectionClass($calledClass);
            self::$constCacheArray[$calledClass] = $reflect->getConstants();
        }
        return self::$constCacheArray[$calledClass];
    }

    public static function getIdName()
    {
        $data = self::get();
        $response = [];
        foreach ($data as $datum) {
            $temp = [];
            $temp["id"] = $datum;
            $temp["name"] = str_replace("_", " ", ucwords($datum, " \t\r\n\f\v/_"));
            array_push($response, $temp);
        }
        return $response;
    }

    public static function isValidName($name, $strict = false)
    {
        $constants = self::get();

        if ($strict) {
            return array_key_exists($name, $constants);
        }

        $keys = array_map('strtolower', array_keys($constants));
        return in_array(strtolower($name), $keys);
    }

    public static function isValidValue($value, $strict = true)
    {
        $values = array_values(self::get());
        return in_array($value, $values, $strict);
    }
}
