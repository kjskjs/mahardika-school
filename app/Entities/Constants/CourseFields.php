<?php

namespace App\Entities\Constants;

class CourseFields extends CommonDatabaseFields
{
    const NAME = "name";
    const START_DATE = "start_date";
    const END_DATE = "end_date";
    const START_TIME = "start_time";
    const END_TIME = "end_time";
    const LEVEL = "level";
    const TEACHER_ID = "teacher_id";
    const DAYS = "days";
}

