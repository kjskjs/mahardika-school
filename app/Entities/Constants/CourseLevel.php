<?php

namespace App\Entities\Constants;


class CourseLevel extends CommonEnum
{
    const NONE = "none";
    const BEGINNER = "beginner";
    const INTERMEDIATE = "intermediate";
    const EXPERT = "expert";
}
