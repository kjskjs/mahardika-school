<?php

namespace App\Entities\Constants;

class CourseStudentFields extends CommonDatabaseFields
{
    const COURSE_ID = "course_id";
    const STUDENT_ID = "student_id";
    const JOIN_DATE = "join_date";
}

