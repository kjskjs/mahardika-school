<?php

namespace App\Entities\Constants;

class StudentFields extends CommonDatabaseFields
{
    const NAME = "name";
    const GRADE = "grade";
    const JOIN_DATE = "join_date";
    const DATE_OF_BIRTH = "date_of_birth";
}

