<?php

namespace App\Entities\Constants;

class SystemActor extends CommonEnum
{
    const SYSTEM = 'system';
    const ADMIN = 'admin';
}
