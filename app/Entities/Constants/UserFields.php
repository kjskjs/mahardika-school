<?php

namespace App\Entities\Constants;

class UserFields extends CommonDatabaseFields
{
    const NAME = "name";
    const EMAIL = "email";
    const PASSWORD = "password";
    const REMEMBER_TOKEN = "remember_token";
    const LEVEL = "level";
    const EMAIL_VERIFIED_AT = "email_verified_at";
}

