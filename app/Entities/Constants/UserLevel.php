<?php

namespace App\Entities\Constants;


class UserLevel extends CommonEnum
{
    const SUPER_ADMIN = "super-admin";
    const ADMIN = "admin";
    const TEACHER = "teacher";
}
