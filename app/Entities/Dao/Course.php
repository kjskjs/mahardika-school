<?php

namespace App\Entities\Dao;

use App\Entities\Constants\CourseFields;
use App\Entities\Constants\CourseStudentFields;
use App\Entities\Constants\UserFields;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        CourseFields::DELETED_AT,
        CourseFields::START_DATE,
        CourseFields::END_DATE,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        CourseFields::NAME,
        CourseFields::START_DATE,
        CourseFields::END_DATE,
        CourseFields::START_TIME,
        CourseFields::END_TIME,
        CourseFields::LEVEL,
        CourseFields::TEACHER_ID,
        CourseFields::DAYS,
        CourseFields::CREATED_BY,
        CourseFields::UPDATED_BY
    ];

    protected $casts = [
        CourseFields::DAYS => "json"
    ];

    public function teacher_detail()
    {
        return $this->belongsTo(User::class, CourseFields::TEACHER_ID, UserFields::ID);
    }

    public function created_by_detail()
    {
        return $this->belongsTo(User::class, CourseFields::CREATED_BY, UserFields::ID);
    }

    public function updated_by_detail()
    {
        return $this->belongsTo(User::class, CourseFields::UPDATED_BY, UserFields::ID);
    }

    public function students()
    {
        return $this->hasMany(CourseStudent::class, CourseStudentFields::COURSE_ID, CourseFields::ID);
    }

}
