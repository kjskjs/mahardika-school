<?php

namespace App\Entities\Dao;

use App\Entities\Constants\CourseFields;
use App\Entities\Constants\CourseStudentFields;
use App\Entities\Constants\StudentFields;
use App\Entities\Constants\UserFields;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseStudent extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        CourseStudentFields::DELETED_AT,
        CourseStudentFields::JOIN_DATE,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        CourseStudentFields::COURSE_ID,
        CourseStudentFields::STUDENT_ID,
        CourseStudentFields::JOIN_DATE,
        CourseStudentFields::CREATED_BY,
        CourseStudentFields::UPDATED_BY
    ];

    public function created_by_detail()
    {
        return $this->belongsTo(User::class, CourseStudentFields::CREATED_BY, UserFields::ID);
    }

    public function updated_by_detail()
    {
        return $this->belongsTo(User::class, CourseStudentFields::UPDATED_BY, UserFields::ID);
    }

    public function student_detail()
    {
        return $this->belongsTo(Student::class, CourseStudentFields::STUDENT_ID, StudentFields::ID);
    }

    public function course_detail()
    {
        return $this->belongsTo(Course::class, CourseStudentFields::COURSE_ID, CourseFields::ID);
    }

}
