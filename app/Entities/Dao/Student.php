<?php

namespace App\Entities\Dao;

use App\Entities\Constants\CourseStudentFields;
use App\Entities\Constants\StudentFields;
use App\Entities\Constants\UserFields;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        StudentFields::DELETED_AT,
        StudentFields::JOIN_DATE,
        StudentFields::DATE_OF_BIRTH,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        StudentFields::NAME,
        StudentFields::GRADE,
        StudentFields::JOIN_DATE,
        StudentFields::DATE_OF_BIRTH,
        StudentFields::CREATED_BY,
        StudentFields::UPDATED_BY
    ];

    public function created_by_detail()
    {
        return $this->belongsTo(User::class, StudentFields::CREATED_BY, UserFields::ID);
    }

    public function updated_by_detail()
    {
        return $this->belongsTo(User::class, StudentFields::UPDATED_BY, UserFields::ID);
    }

    public function courses()
    {
        return $this->hasMany(CourseStudent::class, CourseStudentFields::STUDENT_ID, StudentFields::ID);
    }
}
