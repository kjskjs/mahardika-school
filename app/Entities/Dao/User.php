<?php

namespace App\Entities\Dao;

use App\Entities\Constants\CourseFields;
use App\Entities\Constants\UserFields;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        UserFields::NAME,
        UserFields::EMAIL,
        UserFields::PASSWORD,
        UserFields::REMEMBER_TOKEN,
        UserFields::LEVEL,
        UserFields::CREATED_BY,
        UserFields::UPDATED_BY,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        UserFields::PASSWORD,
        UserFields::REMEMBER_TOKEN,
        UserFields::DELETED_AT,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        UserFields::EMAIL_VERIFIED_AT => 'datetime',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [UserFields::DELETED_AT];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courses()
    {
        return $this->hasMany(Course::class, CourseFields::TEACHER_ID, UserFields::ID);
    }

    public function created_by_detail()
    {
        return $this->belongsTo(User::class, UserFields::CREATED_BY, UserFields::ID);
    }

    public function updated_by_detail()
    {
        return $this->belongsTo(User::class, UserFields::UPDATED_BY, UserFields::ID);
    }
}
