<?php

namespace App\Entities\Dto;

class CommonModelDto
{
    private $_data;

    /**
     * Instantiate a new Dto instance.
     *
     * @param array $properties
     */
    public function __construct(array $properties = [])
    {
        $this->_data = array_fill_keys($properties, null);
    }

    public function __set($property, $value)
    {
        return $this->_data[$property] = $value;
    }

    public function __get($property)
    {
        return array_key_exists($property, $this->_data)
            ? $this->_data[$property] : null;
    }

    public function fill(array $data, bool $strict = false): self
    {
        foreach ($data as $key => $value) {
            if (!$strict || array_key_exists($key, $this->_data)) {
                $this->_data[$key] = $value;
            }
        }
        return $this;
    }

    public static function newFill(array $data, bool $strict = false): self
    {
        return (new static())->fill($data, $strict);
    }

    public function toArray()
    {
        return $this->_data;
    }

    public function toJson()
    {
        return json_encode($this->_data);
    }

}
