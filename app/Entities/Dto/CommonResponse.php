<?php
namespace App\Entities\Dto;

class CommonResponse extends CommonModelDto
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "data",
        "message",
        "status",
        "error",
    ];

    /**
     * Instantiate a new model instance.
     *
     */
    public function __construct()
    {
        parent::__construct($this->fillable);
    }

}
