<?php

namespace App\Entities\Dto;

/**
 *
 */
class PageRequest
{

	private $page;
	private $page_size;
	private $sort;
	private $filter;

	function __construct($page = 0, $page_size = 10, $sort = null, $filter = null)
	{
		$this->page = $page;
		$this->page_size = $page_size;
		$this->sort = $sort;
		$this->filter = $filter;
	}


	public function getPage()
	{
		return $this->page;
	}

	public function getPageSize()
	{
		return $this->page_size;
	}

	public function getSort()
	{
		return $this->sort;
	}

	public function getFilter()
	{
		return $this->filter;
	}

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @param int $page_size
     */
    public function setPageSize(int $page_size): void
    {
        $this->page_size = $page_size;
    }

    /**
     * @param null $sort
     */
    public function setSort($sort): void
    {
        $this->sort = $sort;
    }

    /**
     * @param null $filter
     */
    public function setFilter($filter): void
    {
        $this->filter = $filter;
    }


}
