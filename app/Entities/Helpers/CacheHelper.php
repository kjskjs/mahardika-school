<?php

namespace App\Entity\Helper;

use Illuminate\Support\Facades\Cache;

class CacheHelper
{
    /*
     *  Expiration in second
     */
    public static function set($key, $data, $expiration = 0)
    {
        try {
            $value = json_encode($data);
            if ($expiration == 0) {
                Cache::put($key, $value);
            } else {
                Cache::put($key, $value, $expiration);
            }
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    public static function get($key)
    {
        if (!Cache::has($key)) {
            return null;
        }
        try {
            $data = Cache::get($key);
            return json_decode($data, true);
        } catch (Exception $e) {
            return null;
        }
    }

    public static function forget($key)
    {
        try {
            Cache::forget($key);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    public static function getOrCallAndSet($key, $callback, $expiration = 0, $save_empty = false)
    {
        $data = self::get($key);

        if (!empty($data)) {
            return $data;
        }

        $callbackResponse = $callback();
        if (!empty($callbackResponse) || $save_empty){
            self::set($key, $callbackResponse, $expiration);
        }
        return $callbackResponse;

    }
}
