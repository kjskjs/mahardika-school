<?php


namespace App\Entities\Helpers;


use Illuminate\Support\Facades\Validator;

class CourseStudentHelper
{
    /**
     * Get a validator for an incoming request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function validator(array $data)
    {
        return Validator::make($data, [
            "join_date" => ["required", "date_format:Y-m-d"],
        ]);
    }

}
