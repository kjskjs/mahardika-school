<?php


namespace App\Entities\Helpers;

use App\Entities\Dto\PageRequest;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class QueryHelper
{
    /**
     * @param Request $request
     * @param int $page
     * @param int $perPage
     * @return PageRequest
     */
    public static function generatePageRequest(Request $request, int $page, int $perPage): PageRequest
    {
        $input = $request->all();
        $filter = $request->has('filter') ? $input['filter'] : null;
        $sort = $request->has('sort') ? $input['sort'] : null;

        if ($page <= 0) {
            $page = 1;
        }

        return new PageRequest($page, $perPage, $sort, $filter);
    }

    /**
     * @param PageRequest $pageRequest
     * @param Builder $query
     * @param string $nameField
     * @param array $join
     * @return array
     */
    public static function buildPaginateQuery(PageRequest $pageRequest, Builder $query, string $nameField = "name"): array
    {
        $query = self::constructQuery($pageRequest, $query, $nameField);

        return $query->paginate($pageRequest->getPageSize(), ["*"], "page", $pageRequest->getPage())->toArray();
    }

    /**
     * @param PageRequest $pageRequest
     * @param Builder $query
     * @param string $nameField
     * @return Builder
     */
    private static function constructQuery(PageRequest $pageRequest, Builder $query, string $nameField): Builder
    {
        if (!empty($pageRequest->getFilter())) {
            $query->where($nameField, 'LIKE', '%' . $pageRequest->getFilter() . '%');
        }

        if (!empty($pageRequest->getSort())) {
            $sort = explode('|', $pageRequest->getSort());
            $query->orderBy($sort[0], strtoupper($sort[1]));
        } else {
            $query->orderBy('created_at', 'DESC');
        }
        return $query;
    }
}
