<?php

namespace App\Entities\Helpers;

use App\Entities\Constants\SystemActor;
use App\Repositories\Implementations\UserRepositoryImplementation;

/**
 *
 */
class UserHelper
{
    public static function getMultipleActorDetail($data)
    {
        foreach ($data as $key => $value) {
            $data[$key] = self::getSingleActorDetail($value);
        }

        return $data;
    }

    public static function getSingleActorDetail($data)
    {
        if (isset($data['created_by']) && SystemActor::SYSTEM == $data['created_by']) {
            $data['created_by_detail'] = ["name" => SystemActor::SYSTEM];
        }

        if (isset($data['updated_by']) && SystemActor::SYSTEM == $data['updated_by']) {
            $data['updated_by_detail'] = ["name" => SystemActor::SYSTEM];
        }
        return $data;
    }
}
