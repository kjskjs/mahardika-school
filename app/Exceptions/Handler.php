<?php

namespace App\Exceptions;

use App\Entities\Dto\CommonResponse;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param \Exception $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, Exception $exception)
    {
        if ($request->wantsJson()) {
            return $this->handleApiException($request, $exception);
        }
        return parent::render($request, $exception);
    }

    private function handleApiException($request, Exception $exception)
    {
        $exception = $this->prepareException($exception);

        Log::debug($exception->getMessage());

        if ($exception instanceof NotFoundHttpException) {
            return response(CommonResponse::newFill([
                'error' => ["Not Found"],
                'data' => null,
                'status' => Response::HTTP_NOT_FOUND,
                'message' => Response::$statusTexts[Response::HTTP_NOT_FOUND],
            ])->toArray(), Response::HTTP_NOT_FOUND);
        }

        if ($exception instanceof AuthenticationException) {
            return response(CommonResponse::newFill([
                'error' => [Response::$statusTexts[Response::HTTP_UNAUTHORIZED]],
                'data' => null,
                'status' => Response::HTTP_UNAUTHORIZED,
                'message' => Response::$statusTexts[Response::HTTP_UNAUTHORIZED],
            ])->toArray(), Response::HTTP_UNAUTHORIZED);
        }

        return response(CommonResponse::newFill([
            'error' => ["Whoops! Something went wrong. Error : " . $exception->getMessage()],
            'data' => null,
            'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
            'message' => Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR]
        ])->toArray(), Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
