<?php

namespace App\Http\Controllers\Api\V1\Auths;

use App\Entities\Dto\CommonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class LoginController extends Controller
{
    public static function login(Request $request)
    {
        $response = new CommonResponse();
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()){
            return response($response->fill([
                'message' => Response::$statusTexts[Response::HTTP_BAD_REQUEST],
                'data' => null,
                'status' => Response::HTTP_BAD_REQUEST,
                'error' => $validator->errors()->getMessages(),
            ])->toArray());
        }

        if (!Auth::attempt($request->all())) {
            return response($response->fill([
                'message' => Response::$statusTexts[Response::HTTP_BAD_REQUEST],
                'data' => null,
                'status' => Response::HTTP_BAD_REQUEST,
                'error' => ["Invalid login credential"],
            ])->toArray());
        }

        $accessToken = Auth::user()->createToken('authToken')->accessToken;

        return response($response->fill([
            'message' => "Success",
            'data' => [
                'user' => Auth::user(),
                'access_token' => $accessToken,
            ],
            'status' => Response::HTTP_OK,
        ])->toArray());

    }
}
