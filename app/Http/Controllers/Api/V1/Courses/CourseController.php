<?php

namespace App\Http\Controllers\Api\V1\Courses;

use App\Http\Controllers\Controller;
use App\Services\Interfaces\CourseService;
use App\Services\Interfaces\CourseStudentService;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public $courseService;
    public $courseStudentService;

    /**
     * Instantiate a new controller instance.
     *
     * @param CourseService $courseService
     * @param CourseStudentService $courseStudentService
     */
    public function __construct(CourseService $courseService, CourseStudentService $courseStudentService)
    {
        $this->courseService = $courseService;
        $this->courseStudentService = $courseStudentService;
    }

    /**
     * Get all resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $input = $request->all();
        $page = $request->has("page") ? (int)$input["page"] : 1;

        $perPage = $request->has("per_page") ? (int)$input["per_page"] : 10;
        return response($this->courseService->findPaginateData($request, $page, $perPage)->toArray());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = $this->courseService->store($request);
        return response($response->toArray())->setStatusCode($response->status);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response($this->courseService->findById($id)->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = $this->courseService->update($request, $id);

        return response($response->toArray())->setStatusCode($response->status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = $this->courseService->destroy($id);

        return response($response->toArray())->setStatusCode($response->status);
    }

    /**
     * Assign Student to a Course.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function getStudents($id)
    {
        $response = $this->courseService->getStudents($id);

        return response($response->toArray())->setStatusCode($response->status);
    }

    /**
     * Assign Student to a Course.
     *
     * @param Request $request
     * @param int $id
     * @param $studentId
     * @return \Illuminate\Http\Response
     */
    public function addStudent(Request $request, $id, $studentId)
    {
        $request->request->add([
            "course_id" => $id,
            "student_id" => $studentId,
        ]);
        $response = $this->courseStudentService->store($request);

        return response($response->toArray())->setStatusCode($response->status);
    }

    /**
     * Assign Student to a Course.
     *
     * @param Request $request
     * @param int $id
     * @param $studentId
     * @return \Illuminate\Http\Response
     */
    public function updateStudent(Request $request, $id, $studentId)
    {
        $request->request->add([
            "course_id" => $id,
            "student_id" => $studentId,
        ]);
        $response = $this->courseStudentService->update($request, $id, $studentId);

        return response($response->toArray())->setStatusCode($response->status);
    }

    /**
     * Assign Student to a Course.
     *
     * @param int $id
     * @param $studentId
     * @return \Illuminate\Http\Response
     */
    public function deleteStudent($id, $studentId)
    {
        $response = $this->courseStudentService->destroy($id, $studentId);

        return response($response->toArray())->setStatusCode($response->status);
    }

    /**
     * Assign Student to a Course.
     *
     * @param int $id
     * @param $studentId
     * @return \Illuminate\Http\Response
     */
    public function findStudent($id, $studentId)
    {
        $response = $this->courseStudentService->findById($id, $studentId);

        return response($response->toArray())->setStatusCode($response->status);
    }

}
