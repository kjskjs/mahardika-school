<?php

namespace App\Http\Controllers\Api\V1\Students;

use App\Http\Controllers\Controller;
use App\Services\Interfaces\CourseStudentService;
use App\Services\Interfaces\StudentService;
use Illuminate\Http\Request;

class StudentController extends Controller
{

    public $studentService;
    public $courseStudentService;

    /**
     * Instantiate a new controller instance.
     *
     * @param StudentService $studentService
     * @param CourseStudentService $courseStudentService
     */
    public function __construct(StudentService $studentService, CourseStudentService $courseStudentService)
    {
        $this->studentService = $studentService;
        $this->courseStudentService = $courseStudentService;
    }

    /**
     * Get all resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $input = $request->all();
        $page = $request->has("page") ? (int)$input["page"] : 1;

        $perPage = $request->has("per_page") ? (int)$input["per_page"] : 10;
        return response($this->studentService->findPaginateData($request, $page, $perPage)->toArray());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = $this->studentService->store($request);
        return response($response->toArray())->setStatusCode($response->status);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response($this->studentService->findById($id)->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = $this->studentService->update($request, $id);

        return response($response->toArray())->setStatusCode($response->status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = $this->studentService->destroy($id);

        return response($response->toArray())->setStatusCode($response->status);
    }

    /**
     * Assign Student to a Course.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function getCourses($id)
    {
        $response = $this->studentService->getCourses($id);

        return response($response->toArray())->setStatusCode($response->status);
    }

    /**
     * Assign Student to a Course.
     *
     * @param Request $request
     * @param int $id
     * @param $courseId
     * @return \Illuminate\Http\Response
     */
    public function addCourse(Request $request, $id, $courseId)
    {
        $request->request->add([
            "student_id" => $id,
            "course_id" => $courseId,
        ]);
        $response = $this->courseStudentService->store($request);

        return response($response->toArray())->setStatusCode($response->status);
    }

    /**
     * Assign Student to a Course.
     *
     * @param Request $request
     * @param int $id
     * @param $courseId
     * @return \Illuminate\Http\Response
     */
    public function updateCourse(Request $request, $id, $courseId)
    {
        $request->request->add([
            "student_id" => $id,
            "course_id" => $courseId,
        ]);
        $response = $this->courseStudentService->update($request, $courseId, $id);

        return response($response->toArray())->setStatusCode($response->status);
    }

    /**
     * Assign Student to a Course.
     *
     * @param int $id
     * @param $courseId
     * @return \Illuminate\Http\Response
     */
    public function deleteCourse($id, $courseId)
    {
        $response = $this->courseStudentService->destroy($courseId, $id);

        return response($response->toArray())->setStatusCode($response->status);
    }

    /**
     * Assign Student to a Course.
     *
     * @param int $id
     * @param $courseId
     * @return \Illuminate\Http\Response
     */
    public function findCourse($id, $courseId)
    {
        $response = $this->courseStudentService->findById($courseId, $id);

        return response($response->toArray())->setStatusCode($response->status);
    }

}
