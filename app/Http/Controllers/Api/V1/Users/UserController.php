<?php

namespace App\Http\Controllers\Api\V1\Users;

use App\Http\Controllers\Controller;
use App\Services\Interfaces\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    public $userService;

    /**
     * Instantiate a new controller instance.
     *
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Get all resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $input = $request->all();
        $page = $request->has("page") ? (int)$input["page"] : 1;

        $perPage = $request->has("per_page") ? (int)$input["per_page"] : 10;
        return response($this->userService->findPaginateData($request, $page, $perPage)->toArray());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = $this->userService->store($request);
        return response($response->toArray())->setStatusCode($response->status);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response($this->userService->findById($id)->toArray());
    }

    /**
     * Display user profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return response($this->userService->findById(Auth::id())->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = $this->userService->update($request, $id);

        return response($response->toArray())->setStatusCode($response->status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = $this->userService->destroy($id);

        return response($response->toArray())->setStatusCode($response->status);
    }
}
