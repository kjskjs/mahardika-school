<?php

namespace App\Http\Middleware;

use App\Entities\Constants\UserLevel;
use Closure;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Gate::check(UserLevel::ADMIN) && !Gate::check(UserLevel::SUPER_ADMIN)) {
            abort(Response::HTTP_UNAUTHORIZED);
        }
        return $next($request);
    }
}
