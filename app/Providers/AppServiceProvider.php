<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('self_unique', function ($attribute, $value, $parameters, $validator) {
            return !DB::table($parameters[0])->where("deleted_at", null)->where($attribute, $value)->where("id", "<>", $parameters[1])->exists();
        }, 'The :attribute has already been taken.');

        Validator::extend('unique_if', function ($attribute, $value, $parameters, $validator) {
            return !DB::table($parameters[0])->where("deleted_at", null)->where($attribute, $value)->where($parameters[1], $parameters[2])->exists();
        }, 'The :attribute has already been taken.');

        Validator::extend('unique_if_in', function ($attribute, $value, $parameters, $validator) {
            if (count($parameters) < 3) {
                return false;
            }

            return !DB::table($parameters[0])->where("deleted_at", null)->where($attribute, $value)->whereIn($parameters[1], array_slice($parameters, 2))->exists();
        }, 'The :attribute has already been taken.');

        Validator::extend('unique_if_not', function ($attribute, $value, $parameters, $validator) {
            return !DB::table($parameters[0])->where("deleted_at", null)->where($attribute, $value)->where($parameters[1], "<>", $parameters[2])->exists();
        }, 'The :attribute has already been taken.');

        Validator::extend('unique_if_not_in', function ($attribute, $value, $parameters, $validator) {
            if (count($parameters) < 3) {
                return false;
            }

            return !DB::table($parameters[0])->where("deleted_at", null)->where($attribute, $value)->whereNotIn($parameters[1], array_slice($parameters, 2))->exists();
        }, 'The :attribute has already been taken.');

        Validator::extend('self_unique_if', function ($attribute, $value, $parameters, $validator) {
            return !DB::table($parameters[0])->where("id", $parameters[1])->where("deleted_at", null)->where($attribute, $value)->where($parameters[2], $parameters[3])->exists();
        }, 'The :attribute has already been taken.');

        Validator::extend('self_unique_if_in', function ($attribute, $value, $parameters, $validator) {
            if (count($parameters) < 4) {
                return false;
            }

            return !DB::table($parameters[0])->where("id", $parameters[1])->where("deleted_at", null)->where($attribute, $value)->whereIn($parameters[2], array_slice($parameters, 3))->exists();
        }, 'The :attribute has already been taken.');

        Validator::extend('self_unique_if_not', function ($attribute, $value, $parameters, $validator) {
            return !DB::table($parameters[0])->where("id", $parameters[1])->where("deleted_at", null)->where($attribute, $value)->where($parameters[2], "<>", $parameters[3])->exists();
        }, 'The :attribute has already been taken.');

        Validator::extend('self_unique_if_not_in', function ($attribute, $value, $parameters, $validator) {
            if (count($parameters) < 3) {
                return false;
            }

            return !DB::table($parameters[0])->where("id", $parameters[1])->where("deleted_at", null)->where($attribute, $value)->whereNotIn($parameters[2], array_slice($parameters, 3))->exists();
        }, 'The :attribute has already been taken.');
    }
}
