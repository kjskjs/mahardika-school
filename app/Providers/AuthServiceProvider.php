<?php

namespace App\Providers;

use App\Entities\Constants\UserLevel;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define(UserLevel::SUPER_ADMIN, function ($user) {
            return $user->level == UserLevel::SUPER_ADMIN;
        });

        Gate::define(UserLevel::ADMIN, function ($user) {
            return $user->level == UserLevel::ADMIN || $user->level == UserLevel::SUPER_ADMIN;
        });

        Gate::define(UserLevel::TEACHER, function ($user) {
            return $user->level == UserLevel::TEACHER || $user->level == UserLevel::SUPER_ADMIN;
        });

        if (!$this->app->routesAreCached()) {
            Passport::routes();
        }
    }
}
