<?php

namespace App\Providers;

use App\Repositories\Implementations\CourseRepositoryImplementation;
use App\Repositories\Implementations\StudentRepositoryImplementation;
use App\Repositories\Interfaces\CourseRepository;
use App\Repositories\Interfaces\StudentRepository;
use Illuminate\Support\ServiceProvider;

use App\Repositories\Implementations\UserRepositoryImplementation;
use App\Repositories\Interfaces\UserRepository;

class RepositoryImplementationProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserRepository::class, UserRepositoryImplementation::class);
        $this->app->bind(CourseRepository::class, CourseRepositoryImplementation::class);
        $this->app->bind(StudentRepository::class, StudentRepositoryImplementation::class);
        $this->app->bind(CourseStudentRepository::class, CourseStudentRepositoryImplementation::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
