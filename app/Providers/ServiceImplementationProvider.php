<?php

namespace App\Providers;

use App\Services\Implementations\CourseServiceImplementation;
use App\Services\Implementations\CourseStudentServiceImplementation;
use App\Services\Implementations\StudentServiceImplementation;
use App\Services\Implementations\UserServiceImplementation;
use App\Services\Interfaces\CourseService;
use App\Services\Interfaces\CourseStudentService;
use App\Services\Interfaces\StudentService;
use App\Services\Interfaces\UserService;
use Illuminate\Support\ServiceProvider;

class ServiceImplementationProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserService::class, UserServiceImplementation::class);
        $this->app->bind(CourseService::class, CourseServiceImplementation::class);
        $this->app->bind(StudentService::class, StudentServiceImplementation::class);
        $this->app->bind(CourseStudentService::class, CourseStudentServiceImplementation::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
