<?php

namespace App\Repositories\Implementations;

use App\Entities\Constants\CourseFields;
use App\Entities\Constants\StudentFields;
use App\Entities\Constants\UserFields;
use App\Entities\Dao\Course;
use App\Entities\Dto\PageRequest;
use App\Entities\Helpers\QueryHelper;
use App\Repositories\Interfaces\CourseRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CourseRepositoryImplementation implements CourseRepository
{
    public static function findAllPaginate(PageRequest $pageRequest): array
    {
        $query = Course::query()->with([
            "teacher_detail" => function ($query) {
                $query->select(UserFields::ID, UserFields::NAME);
            },
            "created_by_detail" => function ($query) {
                $query->select(UserFields::ID, UserFields::NAME);
            },
            "updated_by_detail" => function ($query) {
                $query->select(UserFields::ID, UserFields::NAME);
            },
        ]);
        return QueryHelper::buildPaginateQuery($pageRequest, $query, CourseFields::NAME);
    }

    public static function findById($id)
    {
        return Course::query()->with([
            "teacher_detail" => function ($query) {
                $query->select(UserFields::ID, UserFields::NAME);
            },
            "created_by_detail" => function ($query) {
                $query->select(UserFields::ID, UserFields::NAME);
            },
            "updated_by_detail" => function ($query) {
                $query->select(UserFields::ID, UserFields::NAME);
            },
        ])->find($id);
    }

    public static function save($input): bool
    {
        $input[CourseFields::CREATED_BY] = Auth::id();
        $input[CourseFields::UPDATED_BY] = Auth::id();
        Course::query()->create($input);
        return true;
    }

    public static function update($input, $id): bool
    {
        $input[CourseFields::UPDATED_BY] = Auth::id();
        try {
            Course::query()->find($id)->fill($input)->save();
        } catch (\Exception $e) {
            Log::error($e->getMessage(), $e->getTrace());
            return false;
        }
        return true;
    }

    public static function delete($id): bool
    {
        try {
            $supplier = Course::query()->find($id);
            $supplier->update([CourseFields::UPDATED_BY => Auth::id()]);
            $supplier->delete();
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    public static function getStudents($id): array
    {
        return self::findById($id)->students()->with([
            "student_detail" => function ($query) {
                $query->select(StudentFields::ID, StudentFields::NAME);
            },
            "created_by_detail" => function ($query) {
                $query->select(UserFields::ID, UserFields::NAME);
            },
            "updated_by_detail" => function ($query) {
                $query->select(UserFields::ID, UserFields::NAME);
            },
        ])->get()->toArray();
    }
}
