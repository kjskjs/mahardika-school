<?php

namespace App\Repositories\Implementations;

use App\Entities\Constants\CourseFields;
use App\Entities\Constants\CourseStudentFields;
use App\Entities\Constants\StudentFields;
use App\Entities\Constants\UserFields;
use App\Entities\Dao\CourseStudent;
use App\Entities\Dto\PageRequest;
use App\Entities\Helpers\QueryHelper;
use App\Repositories\Interfaces\CourseStudentRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CourseStudentRepositoryImplementation implements CourseStudentRepository
{
    public static function findAllPaginate(PageRequest $pageRequest): array
    {
        $query = CourseStudent::query()->with([
            "student_detail" => function ($query) {
                $query->select(StudentFields::ID, StudentFields::NAME);
            },
            "course_detail" => function ($query) {
                $query->select(CourseFields::ID, CourseFields::NAME);
            },
            "created_by_detail" => function ($query) {
                $query->select(UserFields::ID, UserFields::NAME);
            },
            "updated_by_detail" => function ($query) {
                $query->select(UserFields::ID, UserFields::NAME);
            },
        ]);
        return QueryHelper::buildPaginateQuery($pageRequest, $query, CourseStudentFields::JOIN_DATE);
    }

    public static function findById($courseId, $studentId = null)
    {
        return CourseStudent::query()->with([
            "student_detail" => function ($query) {
                $query->select(StudentFields::ID, StudentFields::NAME);
            },
            "course_detail" => function ($query) {
                $query->select(CourseFields::ID, CourseFields::NAME);
            },
            "created_by_detail" => function ($query) {
                $query->select(UserFields::ID, UserFields::NAME);
            },
            "updated_by_detail" => function ($query) {
                $query->select(UserFields::ID, UserFields::NAME);
            }])
            ->where(CourseStudentFields::COURSE_ID, $courseId)
            ->where(CourseStudentFields::STUDENT_ID, $studentId)
            ->first();
    }

    public static function save($input): bool
    {
        $input[CourseStudentFields::CREATED_BY] = Auth::id();
        $input[CourseStudentFields::UPDATED_BY] = Auth::id();
        CourseStudent::query()->create($input);
        return true;
    }

    public static function update($input, $id): bool
    {
        $input[CourseStudentFields::UPDATED_BY] = Auth::id();
        try {
            CourseStudent::query()->find($id)->fill($input)->save();
        } catch (\Exception $e) {
            Log::error($e->getMessage(), $e->getTrace());
            return false;
        }
        return true;
    }

    public static function delete($id): bool
    {
        try {
            $supplier = CourseStudent::query()->find($id);
            $supplier->update([CourseStudentFields::UPDATED_BY => Auth::id()]);
            $supplier->delete();
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }
}
