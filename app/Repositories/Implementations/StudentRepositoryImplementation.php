<?php

namespace App\Repositories\Implementations;

use App\Entities\Constants\CourseFields;
use App\Entities\Constants\StudentFields;
use App\Entities\Constants\UserFields;
use App\Entities\Dao\Student;
use App\Entities\Dto\PageRequest;
use App\Entities\Helpers\QueryHelper;
use App\Repositories\Interfaces\StudentRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class StudentRepositoryImplementation implements StudentRepository
{
    public static function findAllPaginate(PageRequest $pageRequest): array
    {
        $query = Student::query()->with([
            "created_by_detail" => function ($query) {
                $query->select(UserFields::ID, UserFields::NAME);
            },
            "updated_by_detail" => function ($query) {
                $query->select(UserFields::ID, UserFields::NAME);
            },
        ]);
        return QueryHelper::buildPaginateQuery($pageRequest, $query, StudentFields::NAME);
    }

    public static function findById($id)
    {
        return Student::query()->with([
            "created_by_detail" => function ($query) {
                $query->select(UserFields::ID, UserFields::NAME);
            },
            "updated_by_detail" => function ($query) {
                $query->select(UserFields::ID, UserFields::NAME);
            },
        ])->find($id);
    }

    public static function save($input): bool
    {
        $input[StudentFields::CREATED_BY] = Auth::id();
        $input[StudentFields::UPDATED_BY] = Auth::id();
        Student::query()->create($input);
        return true;
    }

    public static function update($input, $id): bool
    {
        $input[StudentFields::UPDATED_BY] = Auth::id();
        try {
            Student::query()->find($id)->fill($input)->save();
        } catch (\Exception $e) {
            Log::error($e->getMessage(), $e->getTrace());
            return false;
        }
        return true;
    }

    public static function delete($id): bool
    {
        try {
            $supplier = Student::query()->find($id);
            $supplier->update([StudentFields::UPDATED_BY => Auth::id()]);
            $supplier->delete();
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    public static function getCourses($id): array
    {
        return self::findById($id)->courses()->with([
            "course_detail" => function ($query) {
                $query->select(CourseFields::ID, CourseFields::NAME);
            },
            "created_by_detail" => function ($query) {
                $query->select(UserFields::ID, UserFields::NAME);
            },
            "updated_by_detail" => function ($query) {
                $query->select(UserFields::ID, UserFields::NAME);
            },
        ])->get()->toArray();
    }
}
