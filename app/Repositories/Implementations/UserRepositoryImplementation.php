<?php

namespace App\Repositories\Implementations;

use App\Entities\Constants\UserFields;
use App\Entities\Constants\UserLevel;
use App\Entities\Dao\User;
use App\Entities\Dto\PageRequest;
use App\Entities\Helpers\QueryHelper;
use App\Repositories\Interfaces\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class UserRepositoryImplementation implements UserRepository
{
    public static function findAllPaginate(PageRequest $pageRequest): array
    {
        $query = User::query()->with([
            "created_by_detail" => function ($query) {
                $query->select(UserFields::ID, UserFields::NAME);
            },
            "updated_by_detail" => function ($query) {
                $query->select(UserFields::ID, UserFields::NAME);
            },
        ]);
        return QueryHelper::buildPaginateQuery($pageRequest, $query, UserFields::NAME);
    }

    public static function findById($id)
    {
        return User::query()->with([
            "created_by_detail" => function ($query) {
                $query->select(UserFields::ID, UserFields::NAME);
            },
            "updated_by_detail" => function ($query) {
                $query->select(UserFields::ID, UserFields::NAME);
            },
        ])->find($id);
    }

    public static function save($input): bool
    {
        $input["created_by"] = Auth::id();
        $input["updated_by"] = Auth::id();
        User::query()->create($input);
        return true;
    }

    public static function update($input, $id): bool
    {
        $input["updated_by"] = Auth::id();
        try {
            User::query()->find($id)->fill($input)->save();
        } catch (\Exception $e) {
            Log::error($e->getMessage(), $e->getTrace());
            return false;
        }
        return true;
    }

    public static function delete($id): bool
    {
        try {
            $supplier = User::query()->find($id);
            $supplier->update(["updated_by" => Auth::id()]);
            $supplier->delete();
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    public static function getUserLite(array $ids, bool $withTrashed = false): array
    {
        $user = User::query();
        if ($withTrashed) {
            $user = $user->withTrashed();
        }

        return $user->pluck("name", "id")->toArray();
    }

    public static function findTeacherById($id)
    {
        return User::query()->where(UserFields::LEVEL, UserLevel::TEACHER)->find($id);
    }

}
