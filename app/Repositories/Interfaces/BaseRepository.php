<?php

namespace App\Repositories\Interfaces;

use App\Entities\Dto\PageRequest;

interface BaseRepository
{
    public static function findAllPaginate(PageRequest $pageRequest): array;

    public static function findById($id);

    public static function save($input): bool;

    public static function update($input, $id): bool;

    public static function delete($id): bool;
}
