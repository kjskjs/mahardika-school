<?php

namespace App\Repositories\Interfaces;

interface CourseRepository extends BaseRepository
{
    //custom interface goes here
    public static function getStudents($id): array;
}
