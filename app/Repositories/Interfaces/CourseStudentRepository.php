<?php

namespace App\Repositories\Interfaces;

interface CourseStudentRepository extends BaseRepository
{
    //custom interface goes here
    public static function findById($courseId, $studentId = null);
}
