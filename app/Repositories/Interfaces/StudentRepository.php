<?php

namespace App\Repositories\Interfaces;

interface StudentRepository extends BaseRepository
{
    //custom interface goes here
    public static function getCourses($id): array;
}
