<?php

namespace App\Repositories\Interfaces;

interface UserRepository extends BaseRepository
{
    //custom interface goes here
    public static function getUserLite(array $ids, bool $withTrashed = false): array;

    public static function findTeacherById($id);
}
