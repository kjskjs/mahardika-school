<?php

namespace App\Services\Implementations;

use App\Entities\Constants\CourseFields;
use App\Entities\Constants\CourseLevel;
use App\Entities\Constants\DayNames;
use App\Entities\Dto\CommonResponse;
use App\Entities\Helpers\CourseStudentHelper;
use App\Entities\Helpers\QueryHelper;
use App\Entities\Helpers\UserHelper;
use App\Repositories\Implementations\CourseRepositoryImplementation;
use App\Repositories\Implementations\UserRepositoryImplementation;
use App\Services\Interfaces\CourseService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Response;

class CourseServiceImplementation implements CourseService
{

    /**
     * Instantiate a new service instance.
     *
     */
    public function __construct()
    {
    }

    /**
     * Get a validator for an incoming request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private static function validator(array $data)
    {
        return Validator::make($data, [
            "name" => ["required", "string", "max:255"],
            "start_date" => ["required", "date_format:Y-m-d"],
            "end_date" => ["nullable", "date_format:Y-m-d", 'after_or_equal:start_date'],
            "start_time" => ["required", "date_format:H:i:s"],
            "end_time" => ["required", "date_format:H:i:s"],
            'level' => ['nullable', Rule::in(array_values(CourseLevel::get()))],
            "teacher_id" => ["required", "numeric"],
            "days" => ["required", "array", Rule::in(array_values(DayNames::get()))],
        ]);
    }

    public static function findPaginateData(Request $request, int $page, int $perPage): CommonResponse
    {
        $pageRequest = QueryHelper::generatePageRequest($request, $page, $perPage);
        $result = CourseRepositoryImplementation::findAllPaginate($pageRequest);

        $result["data"] = UserHelper::getMultipleActorDetail($result["data"]);

        return CommonResponse::newFill([
            "data" => $result,
            "message" => "Success",
            "status" => Response::HTTP_OK,
        ]);
    }

    public static function findById($id): CommonResponse
    {
        $findResult = self::doFindById($id);
        if ($findResult == null) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => "Course Not Found",
                "status" => Response::HTTP_NOT_FOUND,
                "error" => ["id" => "Course Not Found"],
            ]);
        }

        $data = $findResult->toArray();
        return CommonResponse::newFill([
            "data" => UserHelper::getSingleActorDetail($data),
            "message" => "Success",
            "status" => Response::HTTP_OK,
        ]);
    }

    public static function store(Request $request): CommonResponse
    {
        $input = $request->all();
        $validator = self::validator($input);

        if ($validator->fails()) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => Response::$statusTexts[Response::HTTP_BAD_REQUEST],
                "status" => Response::HTTP_BAD_REQUEST,
                "error" => $validator->errors()->getMessages(),
            ]);
        }

        $data = UserRepositoryImplementation::findTeacherById($input[CourseFields::TEACHER_ID]);
        if (empty($data)) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => "Teacher ID Not Found",
                "status" => Response::HTTP_BAD_REQUEST,
                "error" => ["id" => "Teacher ID Not Found"],
            ]);
        }

        $input[CourseFields::DAYS] = array_unique($input[CourseFields::DAYS]);

        if (!CourseRepositoryImplementation::save($input)) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR],
                "status" => Response::HTTP_INTERNAL_SERVER_ERROR,
                "error" => ["system" => "Failed To Save New Courses"],
            ]);
        }

        return CommonResponse::newFill([
            "data" => null,
            "message" => "Successfully Create New Course",
            "status" => Response::HTTP_OK,
        ]);
    }

    public static function update(Request $request, $id): CommonResponse
    {
        $findResult = self::doFindById($id);
        if ($findResult == null) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => "Course Not Found",
                "status" => Response::HTTP_NOT_FOUND,
                "error" => ["id" => "Course Not Found"],
            ]);
        }

        $input = $request->all();
        $validator = self::validator($input);
        if ($validator->fails()) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => Response::$statusTexts[Response::HTTP_BAD_REQUEST],
                "status" => Response::HTTP_BAD_REQUEST,
                "error" => $validator->errors()->getMessages(),
            ]);
        }

        $input[CourseFields::DAYS] = array_unique($input[CourseFields::DAYS]);

        if (!CourseRepositoryImplementation::update($input, $id)) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR],
                "status" => Response::HTTP_INTERNAL_SERVER_ERROR,
                "error" => ["system" => "Failed To Update Courses"],
            ]);
        }

        return CommonResponse::newFill([
            "data" => null,
            "message" => "Successfully Update Course",
            "status" => Response::HTTP_OK,
        ]);
    }

    public static function destroy($id): CommonResponse
    {
        $data = CourseRepositoryImplementation::findById($id);
        if (empty($data)) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => "Course Not Found",
                "status" => Response::HTTP_NOT_FOUND,
                "error" => ["id" => "Course Not Found"],
            ]);
        }

        if (!CourseRepositoryImplementation::delete($id)) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR],
                "status" => Response::HTTP_INTERNAL_SERVER_ERROR,
                "error" => ["system" => "Failed To Delete Courses"],
            ]);
        }

        return CommonResponse::newFill([
            "data" => null,
            "message" => "Successfully Delete Course",
            "status" => Response::HTTP_OK,
        ]);
    }

    /**
     * @param $id
     * @return CommonResponse
     */
    public static function getStudents($id): CommonResponse
    {
        $findResult = self::doFindById($id);
        if ($findResult == null) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => "Course Not Found",
                "status" => Response::HTTP_NOT_FOUND,
                "error" => ["id" => "Course Not Found"],
            ]);
        }

        return CommonResponse::newFill([
            "data" => CourseRepositoryImplementation::getStudents($id),
            "message" => "Success",
            "status" => Response::HTTP_OK,
        ]);
    }

    private static function doFindById($id)
    {
        if (empty($id)) {
            return null;
        }

        $result = CourseRepositoryImplementation::findById($id);
        if (empty($result)) {
            return null;
        }

        return $result;
    }
}
