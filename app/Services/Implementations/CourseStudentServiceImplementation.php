<?php

namespace App\Services\Implementations;

use App\Entities\Constants\CourseStudentFields;
use App\Entities\Dto\CommonResponse;
use App\Entities\Helpers\CourseStudentHelper;
use App\Entities\Helpers\QueryHelper;
use App\Entities\Helpers\UserHelper;
use App\Repositories\Implementations\CourseStudentRepositoryImplementation;
use App\Services\Interfaces\CourseStudentService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class CourseStudentServiceImplementation implements CourseStudentService
{

    /**
     * Instantiate a new service instance.
     *
     */
    public function __construct()
    {
    }

    /**
     * Get a validator for an incoming request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private static function validator(array $data)
    {
        return Validator::make($data, [
            "join_date" => ["required", "date_format:Y-m-d"],
            "student_id" => ["required", "numeric"],
            "course_id" => ["required", "numeric"],
        ]);
    }

    public static function findPaginateData(Request $request, int $page, int $perPage): CommonResponse
    {
        $pageRequest = QueryHelper::generatePageRequest($request, $page, $perPage);
        $result = CourseStudentRepositoryImplementation::findAllPaginate($pageRequest);

        $result["data"] = UserHelper::getMultipleActorDetail($result["data"]);

        return CommonResponse::newFill([
            "data" => $result,
            "message" => "Success",
            "status" => Response::HTTP_OK,
        ]);
    }

    public static function findById($courseId, $studentId = null): CommonResponse
    {
        $findResult = self::doFindById($courseId, $studentId);
        if ($findResult == null) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => "Course Student Not Found",
                "status" => Response::HTTP_NOT_FOUND,
                "error" => ["id" => "Course Student Not Found"],
            ]);
        }

        $data = $findResult->toArray();
        return CommonResponse::newFill([
            "data" => UserHelper::getSingleActorDetail($data),
            "message" => "Success",
            "status" => Response::HTTP_OK,
        ]);
    }

    public static function store(Request $request): CommonResponse
    {
        $input = $request->all();
        $doValidate = self::doValidateCourseStudentUser($input);
        if ($doValidate != null) {
            return $doValidate;
        }

        if (!CourseStudentRepositoryImplementation::save($input)) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR],
                "status" => Response::HTTP_INTERNAL_SERVER_ERROR,
                "error" => ["system" => "Failed To Assign New Student To Course"],
            ]);
        }

        return CommonResponse::newFill([
            "data" => null,
            "message" => "Successfully To Assign New Student To Course",
            "status" => Response::HTTP_OK,
        ]);
    }

    public static function update(Request $request, $courseId, $studentId = null): CommonResponse
    {
        $input = $request->all();
        $doValidate = self::doValidateCourseStudentUser($input);
        if ($doValidate != null) {
            return $doValidate;
        }

        $findResult = self::doFindById($courseId, $studentId);
        if ($findResult == null) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => "Course Student Not Found",
                "status" => Response::HTTP_NOT_FOUND,
                "error" => ["id" => "Course Student Not Found"],
            ]);
        }

        $data = $findResult->toArray();
        if (!CourseStudentRepositoryImplementation::update([
            CourseStudentFields::JOIN_DATE => $input[CourseStudentFields::JOIN_DATE]
        ], $data['id'])) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR],
                "status" => Response::HTTP_INTERNAL_SERVER_ERROR,
                "error" => ["system" => "Failed To Update Student In Course"],
            ]);
        }

        return CommonResponse::newFill([
            "data" => null,
            "message" => "Successfully Update Student In Course",
            "status" => Response::HTTP_OK,
        ]);
    }

    public static function destroy($courseId, $studentId = null): CommonResponse
    {
        $findResult = self::doFindById($courseId, $studentId);
        if ($findResult == null) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => "Course Student Not Found",
                "status" => Response::HTTP_NOT_FOUND,
                "error" => ["id" => "Course Student Not Found"],
            ]);
        }

        $data = $findResult->toArray();
        if (!CourseStudentRepositoryImplementation::delete($data['id'])) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR],
                "status" => Response::HTTP_INTERNAL_SERVER_ERROR,
                "error" => ["system" => "Failed To Delete Student In Course"],
            ]);
        }

        return CommonResponse::newFill([
            "data" => null,
            "message" => "Successfully Delete Student In Course",
            "status" => Response::HTTP_OK,
        ]);
    }

    private static function doValidateCourseStudentUser($input): ?CommonResponse
    {
        $validator = CourseStudentHelper::validator($input);
        if ($validator->fails()) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => Response::$statusTexts[Response::HTTP_BAD_REQUEST],
                "status" => Response::HTTP_BAD_REQUEST,
                "error" => $validator->errors()->getMessages(),
            ]);
        }

        $courseData = CourseServiceImplementation::findById($input['course_id']);
        if ($courseData->status != Response::HTTP_OK) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => "Course Not Found",
                "status" => Response::HTTP_NOT_FOUND,
                "error" => ["courseId" => "Course Student Not Found"],
            ]);
        }

        $studentData = StudentServiceImplementation::findById($input['student_id']);
        if ($studentData->status != Response::HTTP_OK) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => "Student Not Found",
                "status" => Response::HTTP_NOT_FOUND,
                "error" => ["studentId" => "Student Not Found"],
            ]);
        }

        return null;
    }

    private static function doFindById($courseId, $studentId)
    {
        if (empty($courseId) || empty($studentId)) {
            return null;
        }

        $result = CourseStudentRepositoryImplementation::findById($courseId, $studentId);
        if (empty($result)) {
            return null;
        }

        return $result;
    }
}
