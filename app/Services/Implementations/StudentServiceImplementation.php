<?php

namespace App\Services\Implementations;

use App\Entities\Dto\CommonResponse;
use App\Entities\Helpers\QueryHelper;
use App\Entities\Helpers\UserHelper;
use App\Repositories\Implementations\StudentRepositoryImplementation;
use App\Services\Interfaces\StudentService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class StudentServiceImplementation implements StudentService
{

    /**
     * Instantiate a new service instance.
     *
     */
    public function __construct()
    {
    }

    /**
     * Get a validator for an incoming request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected static function validator(array $data)
    {
        return Validator::make($data, [
            "name" => ["required", "string", "max:255"],
            "join_date" => ["required", "date_format:Y-m-d"],
            "date_of_birth" => ["required", "date_format:Y-m-d"],
            "grade" => ["required", "string", "max:255"],
        ]);
    }

    public static function findPaginateData(Request $request, int $page, int $perPage): CommonResponse
    {
        $pageRequest = QueryHelper::generatePageRequest($request, $page, $perPage);
        $result = StudentRepositoryImplementation::findAllPaginate($pageRequest);

        $result["data"] = UserHelper::getMultipleActorDetail($result["data"]);

        return CommonResponse::newFill([
            "data" => $result,
            "message" => "Success",
            "status" => Response::HTTP_OK,
        ]);
    }

    public static function findById($id): CommonResponse
    {
        $findResult = self::doFindById($id);
        if ($findResult == null) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => "Student Not Found",
                "status" => Response::HTTP_NOT_FOUND,
                "error" => ["id" => "Student Not Found"],
            ]);
        }

        $data = $findResult->toArray();
        return CommonResponse::newFill([
            "data" => UserHelper::getSingleActorDetail($data),
            "message" => "Success",
            "status" => Response::HTTP_OK,
        ]);
    }

    public static function store(Request $request): CommonResponse
    {
        $input = $request->all();
        $validator = self::validator($input);

        if ($validator->fails()) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => Response::$statusTexts[Response::HTTP_BAD_REQUEST],
                "status" => Response::HTTP_BAD_REQUEST,
                "error" => $validator->errors()->getMessages(),
            ]);
        }

        if (!StudentRepositoryImplementation::save($input)) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR],
                "status" => Response::HTTP_INTERNAL_SERVER_ERROR,
                "error" => ["system" => "Failed To Save New Students"],
            ]);
        }

        return CommonResponse::newFill([
            "data" => null,
            "message" => "Successfully Create New Student",
            "status" => Response::HTTP_OK,
        ]);
    }

    public static function update(Request $request, $id): CommonResponse
    {
        $findResult = self::doFindById($id);
        if ($findResult == null) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => "Student Not Found",
                "status" => Response::HTTP_NOT_FOUND,
                "error" => ["id" => "Student Not Found"],
            ]);
        }

        $input = $request->all();
        $validator = self::validator($input);
        if ($validator->fails()) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => Response::$statusTexts[Response::HTTP_BAD_REQUEST],
                "status" => Response::HTTP_BAD_REQUEST,
                "error" => $validator->errors()->getMessages(),
            ]);
        }

        if (!StudentRepositoryImplementation::update($input, $id)) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR],
                "status" => Response::HTTP_INTERNAL_SERVER_ERROR,
                "error" => ["system" => "Failed To Update Students"],
            ]);
        }

        return CommonResponse::newFill([
            "data" => null,
            "message" => "Successfully Update Student",
            "status" => Response::HTTP_OK,
        ]);
    }

    public static function destroy($id): CommonResponse
    {
        $data = StudentRepositoryImplementation::findById($id);
        if (empty($data)) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => "Student Not Found",
                "status" => Response::HTTP_NOT_FOUND,
                "error" => ["id" => "Student Not Found"],
            ]);
        }

        if (!StudentRepositoryImplementation::delete($id)) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR],
                "status" => Response::HTTP_INTERNAL_SERVER_ERROR,
                "error" => ["system" => "Failed To Delete Students"],
            ]);
        }

        return CommonResponse::newFill([
            "data" => null,
            "message" => "Successfully Delete Student",
            "status" => Response::HTTP_OK,
        ]);
    }

    /**
     * @param $id
     * @return CommonResponse
     */
    public static function getCourses($id): CommonResponse
    {
        $findResult = self::doFindById($id);
        if ($findResult == null) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => "Student Not Found",
                "status" => Response::HTTP_NOT_FOUND,
                "error" => ["id" => "Student Not Found"],
            ]);
        }

        return CommonResponse::newFill([
            "data" => StudentRepositoryImplementation::getCourses($id),
            "message" => "Success",
            "status" => Response::HTTP_OK,
        ]);
    }

    private static function doFindById($id)
    {
        if (empty($id)) {
            return null;
        }

        $result = StudentRepositoryImplementation::findById($id);
        if (empty($result)) {
            return null;
        }

        return $result;
    }

}
