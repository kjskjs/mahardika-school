<?php

namespace App\Services\Implementations;

use App\Entities\Constants\UserLevel;
use App\Entities\Dto\CommonResponse;
use App\Entities\Helpers\QueryHelper;
use App\Entities\Helpers\UserHelper;
use App\Repositories\Implementations\UserRepositoryImplementation;
use App\Services\Interfaces\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Response;

class UserServiceImplementation implements UserService
{

    /**
     * Instantiate a new service instance.
     *
     */
    public function __construct()
    {
    }

    /**
     * Get a validator for an incoming request.
     *
     * @param array $data
     * @param null $id
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected static function validator(array $data, $id = null)
    {
        return Validator::make($data, [
            "name" => ["required", "string", "max:255"],
            "email" => ["required", "email", "max:255", "self_unique:users," . $id],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'level' => ['required', Rule::in(array_values(UserLevel::get()))]
        ]);
    }

    public static function findPaginateData(Request $request, int $page, int $perPage): CommonResponse
    {
        $pageRequest = QueryHelper::generatePageRequest($request, $page, $perPage);
        $result = UserRepositoryImplementation::findAllPaginate($pageRequest);

        $result["data"] = UserHelper::getMultipleActorDetail($result["data"]);

        return CommonResponse::newFill([
            "data" => $result,
            "message" => "Success",
            "status" => Response::HTTP_OK,
        ]);
    }

    public static function findById($id): CommonResponse
    {
        if (empty($id)) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => "User Not Found",
                "status" => Response::HTTP_NOT_FOUND,
                "error" => ["id" => "User Not Found"],
            ]);
        }

        $result = UserRepositoryImplementation::findById($id);
        if (empty($result)) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => "User Not Found",
                "status" => Response::HTTP_NOT_FOUND,
                "error" => ["id" => "User Not Found"],
            ]);
        }

        $data = $result->toArray();
        return CommonResponse::newFill([
            "data" => UserHelper::getSingleActorDetail($data),
            "message" => "Success",
            "status" => Response::HTTP_OK,
        ]);
    }

    public static function store(Request $request): CommonResponse
    {
        $input = $request->all();
        $validator = self::validator($input);

        if ($validator->fails()) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => Response::$statusTexts[Response::HTTP_BAD_REQUEST],
                "status" => Response::HTTP_BAD_REQUEST,
                "error" => $validator->errors()->getMessages(),
            ]);
        }

        if (!UserRepositoryImplementation::save($input)) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR],
                "status" => Response::HTTP_INTERNAL_SERVER_ERROR,
                "error" => ["system" => "Failed To Save New Users"],
            ]);
        }

        return CommonResponse::newFill([
            "data" => null,
            "message" => "Successfully Create New User",
            "status" => Response::HTTP_OK,
        ]);
    }

    public static function update(Request $request, $id): CommonResponse
    {
        $data = UserRepositoryImplementation::findById($id);

        if (empty($data)) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => "User Not Found",
                "status" => Response::HTTP_NOT_FOUND,
                "error" => ["id" => "User Not Found"],
            ]);
        }

        $input = $request->all();
        $validator = self::validator($input, $id);
        if ($validator->fails()) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => Response::$statusTexts[Response::HTTP_BAD_REQUEST],
                "status" => Response::HTTP_BAD_REQUEST,
                "error" => $validator->errors()->getMessages(),
            ]);
        }

        if (!UserRepositoryImplementation::update($input, $id)) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR],
                "status" => Response::HTTP_INTERNAL_SERVER_ERROR,
                "error" => ["system" => "Failed To Update Users"],
            ]);
        }

        return CommonResponse::newFill([
            "data" => null,
            "message" => "Successfully Update User",
            "status" => Response::HTTP_OK,
        ]);
    }

    public static function destroy($id): CommonResponse
    {
        $data = UserRepositoryImplementation::findById($id);
        if (empty($data)) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => "User Not Found",
                "status" => Response::HTTP_NOT_FOUND,
                "error" => ["id" => "User Not Found"],
            ]);
        }

        if (!UserRepositoryImplementation::delete($id)) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR],
                "status" => Response::HTTP_INTERNAL_SERVER_ERROR,
                "error" => ["system" => "Failed To Delete Users"],
            ]);
        }

        return CommonResponse::newFill([
            "data" => null,
            "message" => "Successfully Delete User",
            "status" => Response::HTTP_OK,
        ]);
    }

    public static function findByTeacherId($id): CommonResponse
    {
        if (empty($id)) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => "Teacher Not Found",
                "status" => Response::HTTP_NOT_FOUND,
                "error" => ["id" => "Teacher Not Found"],
            ]);
        }

        $result = UserRepositoryImplementation::findById($id);
        if (empty($result)) {
            return CommonResponse::newFill([
                "data" => null,
                "message" => "User Not Found",
                "status" => Response::HTTP_NOT_FOUND,
                "error" => ["id" => "User Not Found"],
            ]);
        }

        $data = $result->toArray();
        return CommonResponse::newFill([
            "data" => UserHelper::getSingleActorDetail($data),
            "message" => "Success",
            "status" => Response::HTTP_OK,
        ]);
    }
}
