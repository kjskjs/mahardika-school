<?php


namespace App\Services\Interfaces;

use App\Entities\Dto\CommonResponse;
use Illuminate\Http\Request;

interface BaseService
{
    public static function findPaginateData(Request $request, int $page, int $perPage): CommonResponse;

    public static function findById($id): CommonResponse;

    public static function store(Request $request): CommonResponse;

    public static function update(Request $request, $id): CommonResponse;

    public static function destroy($id): CommonResponse;
}
