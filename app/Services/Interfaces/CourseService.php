<?php

namespace App\Services\Interfaces;

use App\Entities\Dto\CommonResponse;
use Illuminate\Http\Request;

interface CourseService extends BaseService
{
    //custom interface goes here
    public static function getStudents($id): CommonResponse;
}
