<?php

namespace App\Services\Interfaces;

use App\Entities\Dto\CommonResponse;
use Illuminate\Http\Request;

interface CourseStudentService extends BaseService
{
    //custom interface goes here
    public static function update(Request $request, $courseId, $studentId = null): CommonResponse;
    public static function findById($courseId, $studentId = null): CommonResponse;
    public static function destroy($courseId, $studentId = null): CommonResponse;
}
