<?php

namespace App\Services\Interfaces;

use App\Entities\Dto\CommonResponse;
use Illuminate\Http\Request;

interface StudentService extends BaseService
{
    //custom interface goes here

    public static function getCourses($id): CommonResponse;
}
