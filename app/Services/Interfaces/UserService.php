<?php

namespace App\Services\Interfaces;

use App\Entities\Dto\CommonResponse;

interface UserService extends BaseService
{
    //custom interface goes here
    public static function findByTeacherId($id): CommonResponse;
}
