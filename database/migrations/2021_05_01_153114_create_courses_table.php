<?php

use App\Entities\Constants\CourseFields;
use App\Entities\Constants\CourseLevel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->bigIncrements(CourseFields::ID);
            $table->string(CourseFields::NAME);
            $table->date(CourseFields::START_DATE);
            $table->date(CourseFields::END_DATE)->nullable();
            $table->string(CourseFields::START_TIME);
            $table->string(CourseFields::END_TIME);
            $table->string(CourseFields::LEVEL)->default(CourseLevel::NONE);
            $table->string(CourseFields::TEACHER_ID)->nullable();
            $table->json(CourseFields::DAYS)->default(json_encode([]));
            $table->string(CourseFields::CREATED_BY);
            $table->string(CourseFields::UPDATED_BY);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
