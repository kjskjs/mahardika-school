<?php

use App\Entities\Constants\StudentFields;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements(StudentFields::ID);
            $table->string(StudentFields::NAME);
            $table->string(StudentFields::GRADE);
            $table->date(StudentFields::JOIN_DATE);
            $table->date(StudentFields::DATE_OF_BIRTH);
            $table->string(StudentFields::CREATED_BY);
            $table->string(StudentFields::UPDATED_BY);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
