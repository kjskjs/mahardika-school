<?php

use App\Entities\Constants\CourseStudentFields;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger(CourseStudentFields::COURSE_ID);
            $table->bigInteger(CourseStudentFields::STUDENT_ID);
            $table->date(CourseStudentFields::JOIN_DATE);
            $table->string(CourseStudentFields::CREATED_BY);
            $table->string(CourseStudentFields::UPDATED_BY);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_students');
    }
}
