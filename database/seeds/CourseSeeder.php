<?php

use App\Entities\Constants\SystemActor;
use App\Entities\Constants\CourseLevel;
use App\Entities\Constants\UserLevel;
use App\Entities\Dao\Course;
use App\Entities\Dao\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $generate = 50;
        $fake_name = [
            'English',
            'Japan',
            'Expert',
            'Intermediate',
            'Training',
            'Programming',
            'Sports',
            'Trainee',
            'Gaming',
            'Indonesian',
            'Math',
        ];

        $level = CourseLevel::getIdName();
        $teacher = User::query()->where('level', UserLevel::TEACHER)->pluck('id')->toArray();
        $data = [];

        for ($i = 0; $i < $generate; $i++) {
            $startDate = Carbon::now()->addDays(rand(-1000, 1000));
            array_push($data, [
                "name" => $fake_name[array_rand($fake_name)]. " " . $fake_name[array_rand($fake_name)],
                "start_date" => $startDate->toDateString(),
                "end_date" => (rand(0,1) == 1) ? $startDate->addDays(rand(0, 100))->toDateString() : null,
                "start_time" => sprintf("%02d", rand(0,23)).":".sprintf("%02d", rand(0,59)).":".sprintf("%02d", rand(0,59)),
                "end_time" => sprintf("%02d", rand(0,23)).":".sprintf("%02d", rand(0,59)).":".sprintf("%02d", rand(0,59)),
                "level" => $level[array_rand($level)]["id"],
                "teacher_id" => $teacher[array_rand($teacher)],
                "created_by" => SystemActor::SYSTEM,
                "updated_by" => SystemActor::SYSTEM,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]);
        }

        Course::insert($data);

    }
}
