<?php

use App\Entities\Constants\SystemActor;
use App\Entities\Dao\Course;
use App\Entities\Dao\CourseStudent;
use App\Entities\Dao\Student;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CourseStudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $generate = 100;
        $data = [];
        $checker = [];
        $students = Student::query()->pluck("id")->toArray();
        $courses = Course::query()->pluck("id")->toArray();

        for ($i = 0; $i < $generate; $i++) {
            $courseId = $courses[array_rand($courses)];
            $studentId = $students[array_rand($students)];
            if (!isset($checker[$studentId])) {
                $checker[$studentId] = [];
            }
            while (in_array($courseId, $checker[$studentId])) {
                $courseId = $courses[array_rand($courses)];
                $studentId = $students[array_rand($students)];
            }
            array_push($checker[$studentId], $courseId);
            array_push($data, [
                "course_id" => $courseId,
                "student_id" => $studentId,
                "join_date" => Carbon::now()->addDays(rand(-1000, 1))->toDateString(),
                "created_by" => SystemActor::SYSTEM,
                "updated_by" => SystemActor::SYSTEM,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]);
        }

        CourseStudent::insert($data);

    }
}
