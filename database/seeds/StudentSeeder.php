<?php

use App\Entities\Constants\SystemActor;
use App\Entities\Dao\Student;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $generate = 50;
        $fake_name = [
            'Angelina',
            'Nicole',
            'Čantrić',
            'Heller',
            'Connie',
            'Marks',
            'Jaclyn',
            'Kirlin',
            'Amely',
            'Harvey',
            'Leonor',
            'Pouros',
            'Melissa',
            'Hansen',
            'Jed',
            'Quitzon',
            'Weston',
            'Turcotte',
            'Everette',
            'Conn',
            'Peter',
            'Parker',
            'Guillermo',
            'Rau',
            'Nola',
            'Goodwin',
            'Jessie',
            'Thiel'
        ];

        $data = [];

        for ($i = 0; $i < $generate; $i++) {
            array_push($data, [
                "name" => $fake_name[array_rand($fake_name)]. " " .$fake_name[array_rand($fake_name)]. " " . $fake_name[array_rand($fake_name)],
                "grade" => rand(1,12),
                "join_date" => Carbon::now()->addDays(rand(-1000, 1))->toDateString(),
                "date_of_birth" => Carbon::now()->addDays(rand(-7300, -1825))->toDateString(),
                "created_by" => SystemActor::SYSTEM,
                "updated_by" => SystemActor::SYSTEM,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]);
        }

        Student::insert($data);

    }
}
