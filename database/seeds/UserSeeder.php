<?php

use App\Entities\Constants\SystemActor;
use App\Entities\Constants\UserLevel;
use App\Entities\Dao\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $generate = 35;
        $fake_name = [
            'Angelina',
            'Nicole',
            'Čantrić',
            'Heller',
            'Connie',
            'Marks',
            'Jaclyn',
            'Kirlin',
            'Amely',
            'Harvey',
            'Leonor',
            'Pouros',
            'Melissa',
            'Hansen',
            'Jed',
            'Quitzon',
            'Weston',
            'Turcotte',
            'Everette',
            'Conn',
            'Peter',
            'Parker',
            'Guillermo',
            'Rau',
            'Nola',
            'Goodwin',
            'Jessie',
            'Thiel'
        ];

        $level = UserLevel::getIdName();
        $data = [];

        for ($i = 0; $i < $generate; $i++) {
            array_push($data, [
                "name" => $fake_name[array_rand($fake_name)]. " " . $fake_name[array_rand($fake_name)],
                "email" => Str::random(10) . "@mailinator.com",
                "password" => Hash::make("adminApp123!"),
                "level" => $level[array_rand($level)]["id"],
                "created_by" => SystemActor::SYSTEM,
                "updated_by" => SystemActor::SYSTEM,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]);
        }

        User::insert($data);

    }
}
