<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['cors', 'json.response']], function () {

    /* begin auth routes */
    Route::post('login', 'Api\V1\Auths\LoginController@login');
    /* end auth routes */

    Route::group(['middleware' => ['auth:api']], function () {

        /* begin users routes */
        Route::get('users/profile', 'Api\V1\Users\UserController@profile')->name('users.profile');
        Route::group(['middleware' => ['admin']], function () {
            Route::resource('users', 'Api\V1\Users\UserController');
        });
        /* end users routes */

        Route::group(['middleware' => ['teacher']], function () {
            /* begin courses routes */
            Route::post('courses/{id}/students/{studentId}', 'Api\V1\Courses\CourseController@addStudent')->name('courses.students.store');
            Route::patch('courses/{id}/students/{studentId}', 'Api\V1\Courses\CourseController@updateStudent')->name('courses.students.update');
            Route::delete('courses/{id}/students/{studentId}', 'Api\V1\Courses\CourseController@deleteStudent')->name('courses.students.destroy');
            Route::get('courses/{id}/students/{studentId}', 'Api\V1\Courses\CourseController@findStudent')->name('courses.students.show');
            Route::get('courses/{id}/students', 'Api\V1\Courses\CourseController@getStudents')->name('courses.students.index');

            Route::resource('courses', 'Api\V1\Courses\CourseController');
            /* end courses routes */

            /* begin students routes */
            Route::post('students/{id}/courses/{courseId}', 'Api\V1\Students\StudentController@addCourse')->name('students.courses.store');
            Route::patch('students/{id}/courses/{courseId}', 'Api\V1\Students\StudentController@updateCourse')->name('students.courses.update');
            Route::delete('students/{id}/courses/{courseId}', 'Api\V1\Students\StudentController@deleteCourse')->name('students.courses.destroy');
            Route::get('students/{id}/courses/{courseId}', 'Api\V1\Students\StudentController@findCourse')->name('students.courses.show');
            Route::get('students/{id}/courses', 'Api\V1\Students\StudentController@getCourses')->name('students.courses.index');
            Route::resource('students', 'Api\V1\Students\StudentController');
            /* end courses routes */
        });

    });
});
